

import os
dirname = os.path.abspath(os.path.dirname(__file__))
filename = os.path.join(dirname, 'food_schedule.xlsx')




import pandas as pd


# get all suggestions from excel and place them within a list
def get_suggestions(excel_file):
    read_document = pd.read_excel(excel_file)
    suggestions_list = read_document['Suggestion Contents'].tolist()
    return suggestions_list


suggestions_list = get_suggestions(filename)

def get_user_food_choice():
    while 1 < 2:
        initial_prompt_played = 0
        found = 'no'
        user_food_choice = input('Ποιο φαγητό θα θέλατε να περιλαμβάνει η ημέρα σας; ')
        print('')
        lowercase_user_food_choice = user_food_choice.lower()
        split_user_food_choice = lowercase_user_food_choice.split()
        to_be_printed = []
        for x in split_user_food_choice:
            for y in suggestions_list:
                if x in y:
                    found = 'yes'
                    if y not in to_be_printed:
                        to_be_printed.append(y)
                    if initial_prompt_played == 0:
                        print ('Τα μενού που περιέχουν', x, 'είναι τα ακόλουθα:')
                        print('')
                        initial_prompt_played = 1
        if found == 'yes':
            for z in to_be_printed:
                print(z)
                print('')
            answer = input('Επιθυμείτε να πραγματοποιήσετε αναζήτηση για κάποιο άλλο φαγητό; ')
            if answer == 'ΟΧΙ' or answer == 'Όχι' or answer == 'Οχι' or answer == 'όχι' or answer == 'οχι':
                print('Θα είμαι εδώ όποτε με ξαναχρειαστείτε :)')
                break
        if found == 'no':
            print('Δεν υπάρχουν μενού που να περιέχουν', x, '.')
            answer = input('Επιθυμείτε να πραγματοποιήσετε αναζήτηση για κάποιο άλλο φαγητό; ')
            if answer == 'ΟΧΙ' or answer == 'Όχι' or answer == 'Οχι' or answer == 'όχι' or answer == 'οχι':
                print('Θα είμαι εδώ όποτε με ξαναχρειαστείτε :)')
                break

get_user_food_choice()

